wget https://github.com/nushell/nushell/releases/download/0.74.0/nu-0.74.0-aarch64-unknown-linux-gnu.tar.gz

tar xzf nu-0.74.0-aarch64-unknown-linux-gnu.tar.gz

#patchelf --set-interpreter /lib/ld-linux-aarch64.so.2 ./nu-0.74.0-aarch64-unknown-linux-gnu/nu

cp patchelf ld libc libm
cp config.nu env.nu
echo let-env LD_LIBRARY_PATH = "/lib/aarch64-linux-gnu/" >> .config/nushell/env.nu

tar czf nu-0.74.0-aarch64-unknown-linux-gnu.tar.gz ./nu-0.74.0-aarch64-unknown-linux-gnu

packagename="UserLAnd-Assets-Debian"
curl --progress-bar --header "PRIVATE-TOKEN: CiVFrUHcgbjPRiJtVH9i" \
--upload-file "E:/project/packages/$packagename/latest/nu-0.74.0-aarch64-unknown-linux-gnu.tar.gz" \
"https://gitlab.com/api/v4/projects/30140969/packages/generic/$packagename/latest/nu-0.74.0-aarch64-unknown-linux-gnu.tar.gz?status=default"

rm -R "$HOME"/.local/bin/nu
rm -R "$HOME"/nu-0.72.0-aarch64-unknown-linux-gnu